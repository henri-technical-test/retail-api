import UsersModel from 'models/users.model';
import { getUsers } from 'services/users.service';
import usersMock from 'mocks/users.mock';

describe('getUsers test', () => {
  beforeAll(() => {
    UsersModel.find = jest.fn().mockResolvedValue(usersMock);
  });

  it('should return correct list users', async () => {
    const rslt = await getUsers();
    expect(rslt).toEqual(usersMock);
  });
});
