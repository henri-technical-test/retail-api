import usersModel from 'models/users.model';
import { User } from 'interfaces/users.interface';

export const getUsers: () => Promise<User[]> = async () => {
  const users = await usersModel.find();
  return users;
};
