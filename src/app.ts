// core dependencies
import express from 'express';
import mongoose from 'mongoose';
import dotenv from 'dotenv';
// routers
import usersRoute from 'routes/users.route';

// express config
dotenv.config();
const app = express();
const port = process.env.PORT || 3000;
// mongoose config
const mongoUrl = process.env.MONGO_URL || 'mongodb://127.0.0.1:27017';
const dbName = process.env.DB_NAME || 'retail-api';
const dbString = `${mongoUrl}/${dbName}`;
mongoose.connect(dbString);
const db = mongoose.connection;

// Database connection
db.once('open', (_) => {
  console.log('DB sucessfully connected: ', dbString);
});

db.on('error', (err) => {
  console.error('DB connection error: ', dbString);
});

// App main entries and routes
app.get('/', (req, res) => {
  res.send('Retail API is live.');
});

app.use('/users', usersRoute);

// Init server
app.listen(port, () => {
  console.log(`Retail API listening at http://localhost:${port}`);
});
