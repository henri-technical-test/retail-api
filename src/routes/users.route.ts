import express from 'express';
const usersRoute = express.Router();
// services
import { getUsers } from 'services/users.service';

usersRoute.get('/', async (req, res) => {
  const users = await getUsers();
  res.send(users);
});

export default usersRoute;
