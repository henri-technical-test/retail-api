// create tables
db.createCollection('users');
// add users mock
db.users.insertMany([
  { name: 'Eragon', email: 'eragon@test.com' },
  { name: 'Arya', email: 'arya@test.com' },
  { name: 'Shaphira', email: 'shaphira@test.com' },
  { name: 'Brom', email: 'brom@test.com' },
]);
