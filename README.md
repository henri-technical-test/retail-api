# retail-api

Small API project in NodeJs - Typescript - MongoDB. Theme: Shopping cart management in a automatic retail store.

## Install project

- Copy `.env.sample` to `.env`. Customize your local configs if needed:

  `cp .env.sample .env`

- Install dependencies:

  `yarn` or `npm install`

## Install DB and load mock data

- Install mongoDB on your machine, or run a mongo docker service. Preferred version is `4.4.2`

- Run the following script to load mock data, `retail-api` should be your database name (same as `DB_NAME` in your `.env`)

  `mongo retail-api < ./scripts/prepare-db.js`

## Running dev server, with watch and node debugger

`yarn dev`

## Running production server

`yarn start`

## Running test

`yarn test`
